// Required math library.
exec("TrigonometryStdLib.cs");

// Make %this face %that object's way.
function setFaceAngle(%this, %that, %zAdj)
{
    // %this is the object you want to face a specific direction.
    // %that is the object you want %this to face.
    // %zAdj is a z Rotation Adjustment. Defaults to 0.
    //      This adjusts the rotation so a play may not be looking directly in the direction of %that.
    
    // Fix non-declared value issues.
    if (%zAdj == false) %zAdj = 0;
    
    // get the position of the object we want to face.
    %thatY = getPosition(%that,"y");
    %thatX = getPosition(%that,"x");
    
    // get all the positions of our item we're facing.
    %thisY = getPosition(%that,"y");
    %thisX = getPosition(%that,"x");
    %thisZ = getPosition(%that,"z");
    
    // Setting angle... its weird. Trust me. I know from experience.
    // https://gitlab.com/cmd276/starsiege-map-tool-kit/blob/master/ShapeLogics/circleStdLib.cs
    %angle = getAngle(%thatX, %thatY, %thisX, %thisY);
    if (%angle == "") %angle = 90;
    %angle = %angle + %zAdj;
    
    // Set its location exactly the same, changing only the facing-angle.
    setPosition(%thisX, %thisY, %thisZ, %angle);
}