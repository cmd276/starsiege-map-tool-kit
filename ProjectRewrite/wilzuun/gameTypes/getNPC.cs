
##--------------------------- Header
// FILE:        getNPC.cs
// AUTHORS:     ^TFW^ Wilzuun
// LAST MOD:    Dec 25, 2019
// VERSION:     1.0r

##--------------------------- Version History
// 

##--------------------------- Notes
// Updated from the old DOV script, to allow for on the fly changes to spawned NPC.

##--------------------------- Main Classification
//
//  Turning off either of these will override all options below. Flyers are not
//    included due to them snapping to map locations, and being very hard to
//    predict, and to hit with weapons.
//
//    Examples: tanks = true; hercs = false; Terran = true;
//      Only Terran Tanks will spawn, no Terran hercs will show up.

//  $allow["Tanks"] : boolean
//    true : Enable AI tanks
$allow["Tanks"]     = true;

//  $allow["Hercs"] : boolean
//    true : Enable AI Hercs
$allow["Hercs"]     = true;

//  $allow["Drone"] : boolean
//    true : Enable AI Drones. (These typically have no weapons, so they're pretty much easy kills.
// $allow["Drone"]     = true; // This is Broken. Research required to fix.

// Flyers aren't included at all in this project.

##--------------------------- Faction Classification
//
//  These options matter if the above options are turned on.
//    Example: Tanks = true; hercs = false; Terran = true; Knight = false;
//      Only tanks will appear. Only Terran tanks will be spawned. Knight tanks
//      will not appear at all

//  $allow["Terran"]  : boolean
//    true : Will spawn in Terran vehicles.
$allow["Terran"]    = true;

//  $allow["Knight"]  : boolean
//    true : Will spawn in Knight vehicles.
$allow["Knight"]    = true;

//  $allow["Pirate"]  : boolean
//    true : Will spawn in Pirate vehicles.
$allow["Pirate"]    = true;
$allow["Rebel"]     = true;

//  $allow["Cybrid"]  : boolean
//    true : Will spawn in Cybrid vehicles.
$allow["Cybrid"]    = true;

//  $allow["Metagan"] : boolean
//    true : Will spawn in Metagan vehicles.
$allow["Metagan"]   = true;

//  $allow["Special"] : boolean
//    true : Will spawn in Special vehicles.
$allow["Special"]   = false;

##--------------------------- Specialty Classification
//
//  These last two categories are in respects to groups of vehicles that players
//    may find highly annoying, or undesirable for normal game play. They still 
//    require their Faction Classification to be enabled to show up.

//  $allow["Disruptors"] : boolean
//    true : Will spawn in Disruptor Tanks.
$allow["Disruptors"]= false;

//  $allow["Artillery"] : boolean
//    true : Will spawn in Artillery Tanks.
$allow["Artillery"] = false;



##--------------------------- Functions
// DON'T EDIT BELOW THIS LINE.
function getNPC(%name, %hercs, %tanks, %ter, %kni, %pir, %reb, %cyb, %met, %spe, %dis, %art) 
{
    if (%name == "")
        %name = "Wilzuun";
    if (%hercs == "")
        %hercs = $allow["Hercs"];
    if (%tanks == "")
        %tanks = $allow["tanks"];
    if (%ter == "")
        %ter = $allow["Terran"];
    if (%kni == "")
        %kni = $allow["Knight"];
    if (%pir == "")
        %pir = $allow["Pirate"];
    if (%reb == "")
        %reg = $allow["Rebel"];
    if (%cyb == "")
        %cyb = $allow["Cybrid"];
    if (%met == "")
        %met = $allow["Metagen"];
    if (%spe == "")
        %spe = $allow["Special"];
    if (%dis == "")
        %dis = $allow["Disruptors"];
    if (%art == "")
        %art = $allow["Artillery"];

    %swarmCount = 0;
    %swarmMembers[-1] = true;

    if ($allow["Hercs"] == true)
    {
        if ($allow["Terran"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.1.swarm"; // Terran Apocalypse
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.2.swarm"; // Terran Minotaur
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.3.swarm"; // Terran Gorgon
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.4.swarm"; // Terran Talon
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.5.swarm"; // Terran Basilisk
        }
        if ($allow["Knight"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.10.swarm"; // Knight's Apocalypse
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.11.swarm"; // Knight's Minotaur
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.12.swarm"; // Knight's Gorgon
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.13.swarm"; // Knight's Talon
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.14.swarm"; // Knight's Basilisk
        }
        if ($allow["Cybrid"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.20.swarm"; // Cybrid Seeker
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.21.swarm"; // Cybrid Goad
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.22.swarm"; // Cybrid Shepherd
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.23.swarm"; // Cybrid Adjudicator
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.24.swarm"; // Cybrid Executioner
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.27.swarm"; // Platinum Adjudicator (SP version, not selectable)
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.28.swarm"; // Platinum Executioner (SP version, not selectable)
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.55.swarm"; // Platinum Adjudicator 2
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.56.swarm"; // Platinum Executioner 2
        }
        if ($allow["Metagen"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.35.swarm"; // Metagen Seeker
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.36.swarm"; // Metagen Goad
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.37.swarm"; // Metagen Shepherd
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.38.swarm"; // Metagen Adjudicator
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.39.swarm"; // Metagen Executioner
        }
        if ($allow["Rebel"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.30.swarm"; // Rebel Emancipator
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.33.swarm"; // Rebel Olympian
        }
        if ($allow["Special"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.29.swarm"; // Prometheus
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.40.swarm"; // Harabec's Apocalypse
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.42.swarm"; // Caanan's Basilisk
        }
        if ($allow["Pirate"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.50.swarm"; // Pirate's Apocalypse
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.52.swarm"; // Pirate's Emancipator
        }
    }

    if ($allow["Tanks"] == true)
    {
        if ($allow["Terran"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.6.swarm"; // Paladin Tank
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.7.swarm"; // Myrmidon Tank
            if ($allow["Disruptors"] == true)
            {
                %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.8.swarm"; // Disruptor Tank
            }
            if ($allow["Artillery"] == true)
            {
                %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.133.swarm"; // Nike Artillery
                %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.134.swarm"; // Supressor Tank
            }
        }
        if ($allow["Knight"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.15.swarm"; // Knight's Paladin
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.16.swarm"; // Knight's Myrmidon
            if ($allow["Disruptors"] == true)
            {
                %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.17.swarm"; // Knight's Disruptor
            }
        }
        if ($allow["Cybrid"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.25.swarm"; // Bolo Tank
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.26.swarm"; // Recluse Tank
            if ($allow["Artillery"] == true)
            {
                %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.90.swarm"; // Cybrid Artillery
            }
        }
        if ($allow["Rebel"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.31.swarm"; // Avenger Tank
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.32.swarm"; // Dreadnought Tank
            %swarmMembers[%swarmCount++] = "dov\\vehicles\veh.72.swarm";  // Rebel Thumper
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.138.swarm"; // Rebel bike
            if ($allow["Artillery"] == true)
            {
                %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.137.swarm"; // Rebel Artillery
                %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.150.swarm"; // SUAV Bus
            }
        }
        if ($allow["Special"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.41.swarm"; // Harabec's Predator
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.45.swarm"; // Harabec's Super Predator
        }
        if ($allow["Pirate"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.51.swarm"; // Pirate's Dreadlock
        }
    }

    if ($allow["Drone"] == "Not Happening" && true == false)
    {
        if ($allow["Terran"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.60.swarm";  // Terran Empty Cargo
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.61.swarm";  // Terran Ammo Cargo
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.62.swarm";  // Terran Big Ammo Cargo
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.63.swarm";  // Terran Big Personnel Cargo
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.64.swarm";  // Terran Fuel Cargo
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.65.swarm";  // Terran Minotaur Cargo
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.71.swarm";  // Terran Utility Truck
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.73.swarm";  // Terran Starefield
        }
            if ($allow["Rebel"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.66.swarm";  // Rebel Empty Cargo
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.67.swarm";  // Rebel Ammo Cargo
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.68.swarm";  // Rebel Big Cargo Transport
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.69.swarm";  // Rebel Bix Box Cargo Transport
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.70.swarm";  // Rebel Box Cargo Transport
        }
        if ($allow["Cybrid"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.94.swarm";  // Cybrid Omnicrawler
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.95.swarm";  // Cybrid Protector
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.96.swarm";  // Cybrid Jamma
        }
    }

    %rand = randomInt(1,%swarmCount);
    %obj = %swarmMembers[%rand];
    return loadObject(%name, %obj);
}

