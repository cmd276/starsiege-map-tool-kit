# DOV Read Me File

Install: Drop everything into Multiplayer Folder.

Running: add the line `exec("DOVStdLib.cs");` to the map you wish to run DOV on.

Map Makers: 
    If you wish to change the default values:
        Make a function called `DOVOverride()` and put all the variables you want to customize in your map file.
        The script will load all default values first, then run your Override function, allowing you to override the defualt values to your choosing.

The Settings file is highly commented for map makers to review at their leisure.
The Functionality file is not as commented as the Settings file. Things in the Functionality file should only be changed if you know what you are doing.