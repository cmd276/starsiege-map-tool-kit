##--------------------------- Header
//  Filename: dov\Functionality.cs
//  AUTHOR: ^TFW^ Wilzuun
//  CO-AUTHOR: Drake
##-----------------------------------------------------------------------------
##-----------------------------------------------------------------------------
//      Please don't edit this file.
##-----------------------------------------------------------------------------
##-----------------------------------------------------------------------------

function getAiCount (%player)
{
    return %player.aiCount;
}

function getMax (%player)
{
    return %player.maxAi;
}

function getSpawn (%player)
{
    return %player.sinceSpawn;
}

function initScoreBoard()
{
   deleteVariables("$ScoreBoard::PlayerColumn*");
   deleteVariables("$ScoreBoard::TeamColumn*");
	   // Player ScoreBoard column headings
	   $ScoreBoard::PlayerColumnHeader1 = "AI Count";
	   $ScoreBoard::PlayerColumnHeader2 = *IDMULT_SCORE_KILLS;
	   $ScoreBoard::PlayerColumnHeader3 = *IDMULT_SCORE_DEATHS;
	   $ScoreBoard::PlayerColumnHeader4 = "Kills since Spawn";
	   $ScoreBoard::PlayerColumnHeader5 = "Max Kills";

	   // Player ScoreBoard column functions
	   $ScoreBoard::PlayerColumnFunction1 = "getAiCount";
	   $ScoreBoard::PlayerColumnFunction2 = "getKills";
	   $ScoreBoard::PlayerColumnFunction3 = "getDeaths";
	   $ScoreBoard::PlayerColumnFunction4 = "getSpawn";
	   $ScoreBoard::PlayerColumnFunction5 = "getMax";
       
   // tell server to process all the scoreboard definitions defined above

   serverInitScoreBoard();
}

function onMissionEnd()
{
    echo("## ------------ function onMissionEnd()");
    %players = playerManager::getPlayerCount();
    for(%i = 0; %i < %players; %i++)
    {
        %player = playerManager::getPlayerNum(%i);
        deleteObject(%player.group);
    }
}

function onMissionLoad()
{
    // echo("## ------------ function onMissionLoad()");
    %players = playerManager::getPlayerCount();
    for(%i = 0; %i < %players; %i++)
    {
        %player = playerManager::getPlayerNum(%i);
        %player.sinceSpawn = 0;
        %player.aiCount = 0;
        %player.joinAi = 0;
        %player.maxAi = 0;
        %player.group = newObject("group" @ %player, simGroup);
        addToSet("MissionGroup", %player.group);
    }
}

function GetKilled()
{
    %playerCount = playerManager::getPlayerCount();
    for(%p = 0; %p <%playerCount; %p++)
    {
        %player = playerManager::getPlayerNum(%p);
        %vehicle = playerManager::playerNumToVehicleId(%player);
        if (%player.joinAi == 1)
        {
            %player.joinAi--;
            setTeam(%vehicle, *IDSTR_TEAM_RED);
            say(%player, 0, "Your gace period is over with... The swarm is after you again");
            schedule("order(" @ %player.group @ ", attack, " @ %vehicle @ ");", 1);
        }
        else if (%player.joinAi > 1)
        {
            %player.joinAi--;
        }
    }
    schedule("getKilled();", 1);
}

function onMissionStart()
{
    // echo("## ------------ function onMissionStart()");
    ##  Yellow Team are bots. Make sure they hate red.
    setHostile(*IDSTR_TEAM_YELLOW, *IDSTR_TEAM_RED);
    ## Players should be red. Make sure every one hates them.
    ## Allow them to attack any one.
    setHostile(*IDSTR_TEAM_RED, *IDSTR_TEAM_RED, *IDSTR_TEAM_YELLOW, *IDSTR_TEAM_BLUE, *IDSTR_TEAM_PURPLE);
    setHostile(*IDSTR_TEAM_BLUE, *IDSTR_TEAM_RED);
    setHostile(*IDSTR_TEAM_PURPLE, *IDSTR_TEAM_RED);
}

function player::onAdd(%player)
{
    // echo("## ------------ function player::onAdd()");

    ## Stealth Setting Detection.
    if(($stealthDetect) && (strAlign(1, left, getName(%player)) == "/"))
        say(Everyone, 0, "<F4>" @ getName(%player) @ "<F4> joined the game.");

    ## Set player information.
    ## Max amount of AAI killed before own death.
    %player.maxAi = 0;
    ## Kills since we spawned.
    %player.sinceSpawn = 0;
    ## We have this many AI belonging to this player on the field at this time.
    %player.aiCount = 0;
    ## This player killed another player, and are free to roam.
    %player.joinAi = 0;

    ## Group name: group2049, etc.
    %player.group = newObject("group" @ %player, simGroup);
    addToSet("MissionGroup", %player.group);
}

function player::onRemove(%player)
{
    // echo("## ------------ function player::onRemove()");
    deleteObject(%player.group);
}

function setDefaultMissionOptions()
{
    // echo("## ------------ function setDefaultMissionOptions()");
    $server::TeamPlay = false;
    $server::AllowDeathmatch = true;
    $server::AllowTeamPlay = false;
}

function setRules()
{
    %rules = "Rawr";
    setGameInfo(%rules);      
}
setRules();

function vehicle::onAdd(%vehicleId)
{
    // echo("## ------------ function vehicle::onAdd()");
    %player = playerManager::vehicleIdToPlayerNum(%vehicleId);
    ## Anti-AI check.
    if (%player == 0) return;
    
    if (%player.aiCount <= 0)
    {
        for(%i=0; %i < swarmVolume(%player); %i++)
            spawnSwarm(%player);
    }
    else
    {
        schedule("order(" @ %player.group @ ", attack, " @ %vehicleId @ ");", 0.5);
    }
}

function vehicle::onDestroyed(%destroyed, %destroyer)
{
    // echo("## ------------ function vehicle::onDestroyed()");
    %message = getFancyDeathMessage(getHUDName(%destroyed), getHUDName(%destroyer));
    if(%message != "")
        say( 0, 0, %message);
    vehicle::onDestroyedLog(%destroyed, %destroyer);

    %dead = playerManager::vehicleIdToPlayerNum(%destroyed);
    %living = playerManager::vehicleIdToPlayerNum(%destroyer);

    ## if an AI Dies, schedule its deletion.
    // AI killed AI... oops?
    if ((%living == 0) && (%dead == 0))
    {
        %destroyed.owner.aiCount--;
    }
    // AI kills Player.
    else if ((%living == 0) && (%dead != 0))
    {
        playerDied(%dead);
    }
    // Player Kills Player
    else if ((%living != 0) && (%dead != 0) && (%dead != %living))
    {
        playerLived(%living,true);
        playerDied(%dead);
    }

    // Player Kills AI
    else if ((%living != 0) && (%dead == 0))
    {
        if (getTeam(%destroyed) == getTeam(%destroyer))
        {
            %living.joinAi = 1;
        }

        %destroyed.owner.aiCount--;
    
        %countToSpawn = swarmVolume(%living);
        for(%i = 0; %i < %countToSpawn; %i++)
        {
            // echo("Spawning things");
            spawnSwarm(%living);
        }
        playerLived(%living);
    }
  
    // We dont know any more... Or any thing...
    else 
    {
        // uh oh...
        echo("UNKNOWN DEATH SET");
    }
}

function playerLived(%player, %pvp) 
{
    %destroyer = playerManager::playerNumtoVehicleId(%player);
    // Do we reload them still?
    if (($pveReward) && ($pveReload) && ($pveReloadWhen >= %player.sinceSpawn))
        reloadObject(%destroyer, 99999);
    
    // Do we still heal them?
    if (($pveReward) && ($pveHeal) && ($pveHealWhen >= %player.sinceSpawn))
        healObject(%destroyer,99999);

    if (%pvp)
        if (($pvpReward)&&($pvpStopAi)) // how about joining the swarm, instead of being eaten?
        {
            %player.joinAi = %player.joinAi + $pvpTimeAi; // Extend, or give time.
            setTeam(%destroyer, *IDSTR_TEAM_YELLOW);
            schedule("order(" @ %player.group @ ", guard, " @ %destroyer @ ");", 0.5);
            say(%player, 0, "The swarm is willing to overlook your rogue likeness for a period...");
        }
    %player.sinceSpawn++;
}

function playerDied(%player)
{
    if (%player.sinceSpawn > %player.maxAi)
    {
        %player.maxAi = %player.sinceSpawn;
        say(%player, 0, "New Personal Best! " @ %player.sinceSpawn @ " kills before death");
        // storeScore(%player, %player.sinceSpawn);
    }
    %player.sinceSpawn = 0;
}

function spawnSwarm(%player)
{
    // echo("## ------------ function spawnSwarm()");
    ## If AI, quit.
    if (%player == 0) return;
    ## Get the players vehicle ID for later use.
    %vehicleId = playerManager::playerNumToVehicleId(%player);

    %x1 = getPosition(%vehicleId, x) - 1500;
    %x2 = getPosition(%vehicleId, x) + 1500;
    %y1 = getPosition(%vehicleId, y) - 1500;
    %y2 = getPosition(%vehicleId, y) + 1500;
    
    //%newSpawn = loadObject("Swarm Member", getSwarmMember());
    // if (%vehicleId != "")
    // {
        // %newSpawn = cloneVehicle(%vehicleId);
    // }
    // else 
    // {
        // storeObject(%vehicleId, %vehicleId @ ".veh");
        // %newSpawn = loadObject(%vehicleId @ ".veh");
    // }
    
    if ($swarmClone == true)
    {
        storeObject(%vehicleId, %vehicleId @ ".veh");
        %newSpawn = loadObject(%vehicleId @ ".veh");
    }
    else
    {
        %newSpawn = loadObject("Swarm Member", getSwarmMember());
    }

    %newSpawn.owner = %player;
    setPilotId(%newSpawn, 29);
    setTeam(%newSpawn, *IDSTR_TEAM_YELLOW);
    addToSet("MissionGroup\\group" @ %player, %newSpawn);
    order(%newSpawn, attack, %vehicleId);
    order(%newSpawn, speed, high);
    randomTransport(%newSpawn, %x1, %y1, %x2, %y2);
    
    %player.aiCount++;
}

Pilot Harabec
{
   id = 29;
   
   name = Harabec;
   skill = 1.0;
   accuracy = 1.0;
   aggressiveness = 0.9;
   activeDist = 1500.0;
   deactiveBuff = 2000.0;
   targetFreq = 5.0;
   trackFreq = 1.0;
   fireFreq = 0.2;
   LOSFreq = 0.1;
   orderFreq = 0.2;    
};

function swarmVolume(%player)
{
    // echo("## ------------ function swarmVolume()");
    if ($fixedSwarm == true)
        return $swarmStatic;
    else if ($fixedSwarm == false)
        return floor(%player.sinceSpawn / $swarmDynanic) + 1;
}

function getSwarmMember() 
{
    %swarmCount = 0;
    %swarmMembers[-1] = true;

    if ($allow["Hercs"] == true)
    {
        if ($allow["Terran"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.1.swarm"; // Terran Apocalypse
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.2.swarm"; // Terran Minotaur
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.3.swarm"; // Terran Gorgon
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.4.swarm"; // Terran Talon
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.5.swarm"; // Terran Basilisk
        }
        if ($allow["Knight"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.10.swarm"; // Knight's Apocalypse
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.11.swarm"; // Knight's Minotaur
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.12.swarm"; // Knight's Gorgon
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.13.swarm"; // Knight's Talon
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.14.swarm"; // Knight's Basilisk
        }
        if ($allow["Cybrid"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.20.swarm"; // Cybrid Seeker
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.21.swarm"; // Cybrid Goad
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.22.swarm"; // Cybrid Shepherd
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.23.swarm"; // Cybrid Adjudicator
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.24.swarm"; // Cybrid Executioner
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.27.swarm"; // Platinum Adjudicator (SP version, not selectable)
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.28.swarm"; // Platinum Executioner (SP version, not selectable)
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.55.swarm"; // Platinum Adjudicator 2
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.56.swarm"; // Platinum Executioner 2
        }
        if ($allow["Metagen"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.35.swarm"; // Metagen Seeker
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.36.swarm"; // Metagen Goad
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.37.swarm"; // Metagen Shepherd
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.38.swarm"; // Metagen Adjudicator
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.39.swarm"; // Metagen Executioner
        }
        if ($allow["Rebel"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.30.swarm"; // Rebel Emancipator
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.33.swarm"; // Rebel Olympian
        }
        if ($allow["Special"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.29.swarm"; // Prometheus
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.40.swarm"; // Harabec's Apocalypse
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.42.swarm"; // Caanan's Basilisk
        }
        if ($allow["Pirate"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.50.swarm"; // Pirate's Apocalypse
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.52.swarm"; // Pirate's Emancipator
        }
    }

    if ($allow["Tanks"] == true)
    {
        if ($allow["Terran"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.6.swarm"; // Paladin Tank
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.7.swarm"; // Myrmidon Tank
            if ($allow["Disruptors"] == true)
            {
                %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.8.swarm"; // Disruptor Tank
            }
            if ($allow["Artillery"] == true)
            {
                %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.133.swarm"; // Nike Artillery
                %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.134.swarm"; // Supressor Tank
            }
        }
        if ($allow["Knight"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.15.swarm"; // Knight's Paladin
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.16.swarm"; // Knight's Myrmidon
            if ($allow["Disruptors"] == true)
            {
                %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.17.swarm"; // Knight's Disruptor
            }
        }
        if ($allow["Cybrid"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.25.swarm"; // Bolo Tank
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.26.swarm"; // Recluse Tank
            if ($allow["Artillery"] == true)
            {
                %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.90.swarm"; // Cybrid Artillery
            }
        }
        if ($allow["Rebel"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.31.swarm"; // Avenger Tank
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.32.swarm"; // Dreadnought Tank
            %swarmMembers[%swarmCount++] = "dov\\vehicles\veh.72.swarm";  // Rebel Thumper
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.138.swarm"; // Rebel bike
            if ($allow["Artillery"] == true)
            {
                %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.137.swarm"; // Rebel Artillery
                %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.150.swarm"; // SUAV Bus
            }
        }
        if ($allow["Special"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.41.swarm"; // Harabec's Predator
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.45.swarm"; // Harabec's Super Predator
        }
        if ($allow["Pirate"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.51.swarm"; // Pirate's Dreadlock
        }
    }

    if ($allow["Drone"] == "Not Happening" && true == false)
    {
        if ($allow["Terran"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.60.swarm";  // Terran Empty Cargo
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.61.swarm";  // Terran Ammo Cargo
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.62.swarm";  // Terran Big Ammo Cargo
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.63.swarm";  // Terran Big Personnel Cargo
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.64.swarm";  // Terran Fuel Cargo
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.65.swarm";  // Terran Minotaur Cargo
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.71.swarm";  // Terran Utility Truck
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.73.swarm";  // Terran Starefield
        }
            if ($allow["Rebel"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.66.swarm";  // Rebel Empty Cargo
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.67.swarm";  // Rebel Ammo Cargo
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.68.swarm";  // Rebel Big Cargo Transport
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.69.swarm";  // Rebel Bix Box Cargo Transport
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.70.swarm";  // Rebel Box Cargo Transport
        }
        if ($allow["Cybrid"] == true)
        {
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.94.swarm";  // Cybrid Omnicrawler
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.95.swarm";  // Cybrid Protector
            %swarmMembers[%swarmCount++] = "dov\\vehicles\\veh.96.swarm";  // Cybrid Jamma
        }
    }

    %rand = randomInt(1,%swarmCount);
    return %swarmMembers[%rand];
}

function isItOn (%v)
{
    if (%v == true)
        return "on";
    else
        return "off";
}